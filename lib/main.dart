import 'package:flutter/material.dart';
import 'package:flutter_application_1_test/secondpage.dart';

import 'web_view_container.dart';

Future<void> main() async {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      theme: ThemeData(
        fontFamily: 'Minimal',
        primarySwatch: Colors.deepOrange,
      ),
      home: MyHomePage(),
      routes: <String, WidgetBuilder>{
        '/secondpage': (BuildContext context) => new Secondpage(),
      },
    );
  }
}

class MyHomePage extends StatefulWidget {
  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  int _counter = 0;

  void _incrementCounter() {
    setState(() {
      _counter++;
    });
  }

  final myController = TextEditingController();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title:
            Text("สวัสดี", style: TextStyle(fontSize: 30, color: Colors.white)),
        actions: <Widget>[
        ],
      ),
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            Text(
              'please enter your URL to preview\n(if null will navigate to https://flutter.dev/',
              style: TextStyle(fontSize: 18, color: Colors.blue),
            ),
            Padding(
              padding: const EdgeInsets.all(16.0),
              child: TextField(
                controller: myController,
              ),
            ),
            IconButton(
              icon: Icon(
                Icons.navigate_next,
                color: Colors.black,
              ),
              onPressed: () {
                var url = myController.text;
                if(myController.text == '' || myController.text == null) {
                  url = "https://flutter.dev/";
                }
                            
                Navigator.push(
                  context,
                  MaterialPageRoute(
                    builder: (context) => MyWebViewContainer(
                        url: url),
                  ),
                );
              },
            )
          ],
        ),
      ),
    );
  }
}
