import 'dart:convert';
import 'dart:io' show Platform;
import 'package:flutter/material.dart';
import 'package:adaptive_dialog/adaptive_dialog.dart';
import 'package:flutter_application_1_test/web_view_container.dart';
import 'package:flutter_application_1_test/main.dart';
import 'package:webview_flutter/webview_flutter.dart';
import 'web_view_container.dart';
import 'package:flutter_application_1_test/notificationstest.dart';

// ios need to add the key io.flutter.embedded_views_preview as true in the Info.plist

class Secondpage extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      theme: ThemeData(
        fontFamily: 'Minimal',
        primarySwatch: Colors.deepOrange,
      ),
      home: MySecondpage(),
      routes: <String, WidgetBuilder>{
        '/Notificationspage': (BuildContext context) =>
            new LocalNotificationspage(),
      },
    );
  }
}

class MySecondpage extends StatefulWidget {
  MySecondpage({Key key}) : super(key: key);
  @override
  _MyHomeSecondpage createState() => _MyHomeSecondpage();
}

class _MyHomeSecondpage extends State<MySecondpage> {
  void initState() {
    super.initState();
    // Enable hybrid composition.
    if (Platform.isAndroid) WebView.platform = SurfaceAndroidWebView();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Row(
          mainAxisAlignment: MainAxisAlignment.end,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: <Widget>[
            IconButton(
                icon: Icon(Icons.navigate_before),
                onPressed: () {
                  Navigator.of(context, rootNavigator: true).pop(context);
                }),
            Expanded(
              child: Center(
                  child: Text('   รายการ   ',
                      style: TextStyle(fontSize: 30, color: Colors.black))),
            )
          ],
        ),
        automaticallyImplyLeading: false,
        centerTitle: false,
        actions: <Widget>[
          Row(
            children: <Widget>[
              IconButton(icon: Icon(Icons.shopping_cart), onPressed: () {}),
            ],
          ),
          Row(
            children: <Widget>[
              IconButton(
                  icon: Icon(Icons.notification_important),
                  onPressed: () {
                    Navigator.of(context).pushNamed('/Notificationspage');
                  }),
            ],
          )
        ],
      ),
      body: new ListView(children: [
        new Card(
          clipBehavior: Clip.antiAlias,
          child: new Column(
            children: [
              ListTile(
                subtitle: Text('รายการที่ 1',
                    style: TextStyle(fontSize: 30, color: Colors.black)),
              ),
              Align(
                  alignment: Alignment.centerLeft,
                  child: Padding(
                    padding: EdgeInsets.all(15.0),
                    child: Text(
                      ' ข้อมูลของ รายการที่ 1',
                      style: TextStyle(fontSize: 20, color: Colors.black),
                    ),
                  )),
              ButtonBar(
                alignment: MainAxisAlignment.end,
                children: [
                  Row(
                    children: [
                      new FlatButton(
                        onPressed: () {
                          Navigator.push(
                            context,
                            MaterialPageRoute(
                              builder: (context) => MyWebViewContainer(
                                url: Uri.dataFromString(
                                        '<!DOCTYPE html><html lang="en"><meta charset="UTF-8"><button onclick="Print.postMessage(\'สวัสดี นี้คือ Postmessage (encoding UTF-8)\');">Test sent Message</button></html>',
                                        mimeType: 'text/html',
                                        encoding: Encoding.getByName('utf-8'))
                                    .toString(),
                              ),
                            ),
                          );
                        },
                        child: const Text('รายละเอียด',
                            style:
                                TextStyle(fontSize: 15, color: Colors.black)),
                      ),
                      new FlatButton(
                        onPressed: () {
                          Navigator.push(
                            context,
                            MaterialPageRoute(
                              builder: (context) => MyWebViewContainer(
                                  url: "https://www.google.com"),
                            ),
                          );
                        },
                        child: const Text('google!',
                            style:
                                TextStyle(fontSize: 15, color: Colors.green)),
                      ),
                    ],
                  )
                ],
              ),
              //Image.asset('assets/card-sample-image.jpg'),
            ],
          ),
        ),
      ]),
    );
  }
}
