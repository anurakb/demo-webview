import 'dart:io' show Platform;
import 'dart:convert';
import 'package:flutter/material.dart';
import 'dart:async';
import 'dart:convert';
import 'package:adaptive_dialog/adaptive_dialog.dart';
import 'package:flutter_application_1_test/secondpage.dart';
// import 'package:flutter_local_notifications/flutter_local_notifications.dart';

// FlutterLocalNotificationsPlugin flutterLocalNotificationsPlugin =
//     FlutterLocalNotificationsPlugin();

class LocalNotificationspage extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'LocalNotifications',
      theme: ThemeData(
        fontFamily: 'Minimal',
        primarySwatch: Colors.deepOrange,
      ),
      home: MyLocalNotifications(),
      routes: <String, WidgetBuilder>{
        '/secondpage': (BuildContext context) => new Secondpage(),
      },
    );
  }
}

class MyLocalNotifications extends StatefulWidget {
  MyLocalNotifications({Key key}) : super(key: key);
  @override
  _MyHomeMyLocalNotifications createState() => _MyHomeMyLocalNotifications();
}

String message;
String channelId = "1000";
String channelName = "FLUTTER_NOTIFICATION_CHANNEL";
String channelDescription = "FLUTTER_NOTIFICATION_CHANNEL_DETAIL";

class _MyHomeMyLocalNotifications extends State<MyLocalNotifications> {
  void initState() {
    // message = "No message.";

    // var initializationSettingsAndroid =
    //     AndroidInitializationSettings('ic_launcher');

    // var initializationSettingsIOS = IOSInitializationSettings(
    //     onDidReceiveLocalNotification: (id, title, body, payload) {
    //   print("onDidReceiveLocalNotification called.");
    // });
    // var initializationSettings = InitializationSettings(
    //     android: initializationSettingsAndroid, iOS: initializationSettingsIOS);

    // flutterLocalNotificationsPlugin.initialize(initializationSettings,
    //     onSelectNotification: (payload) {
    //   // when user tap on notification.
    //   print("onSelectNotification called.");
    //   setState(() {
    //     message = payload;
    //   });
    // });
    super.initState();
    // Enable hybrid composition.
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Row(
          mainAxisAlignment: MainAxisAlignment.end,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: <Widget>[
            IconButton(
                icon: Icon(Icons.navigate_before),
                onPressed: () {
                  Navigator.of(context).pushNamed('/secondpage');
                }),
            Expanded(
              child: Center(
                  child: Text(' Notifications ',
                      style: TextStyle(fontSize: 30, color: Colors.black))),
            )
          ],
        ),
        automaticallyImplyLeading: false,
        centerTitle: false,
        actions: <Widget>[
          Row(
            children: <Widget>[
              IconButton(
                  icon: Icon(Icons.notification_important),
                  onPressed: () {
                    //sendNotification();
                  }),
            ],
          )
        ],
      ),
    );
  }
}

// sendNotification() async {
//   var androidPlatformChannelSpecifics = AndroidNotificationDetails('10000',
//       'FLUTTER_NOTIFICATION_CHANNEL', 'FLUTTER_NOTIFICATION_CHANNEL_DETAIL',
//       importance: Importance.max, priority: Priority.high);
//   var iOSPlatformChannelSpecifics = IOSNotificationDetails();

//   var platformChannelSpecifics = NotificationDetails(
//       android: androidPlatformChannelSpecifics,
//       iOS: iOSPlatformChannelSpecifics);

//   await flutterLocalNotificationsPlugin.show(111, 'Hello, benznest.',
//       'This is a your notifications. ', platformChannelSpecifics,
//       payload: 'I just haven\'t Met You Yet');
// }
